##To run the demo 

```
docker-compose up -d --scale whoami=4
```

## To test

```
curl -H Host:whoami.docker.localhost http://127.0.0.1
```